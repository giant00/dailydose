package log

import androidx.room.Embedded
import androidx.room.Relation
import io.calimbak.data.supplement.Supplement

data class SupplementWithLog(
    @Embedded val supplement: Supplement,
    @Relation(
          parentColumn = "uid",
          entityColumn = "supplementId"
    )
    val logs: List<Log>,
)