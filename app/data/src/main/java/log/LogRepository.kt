package log

import io.calimbak.data.supplement.SupplementDao
import kotlinx.coroutines.flow.Flow

class LogRepository(
    private val logDao: LogDao,
    private val supplementDao: SupplementDao,
) {

    suspend fun save(
        id: Long? = null,
        supplementId: Long,
        time: Long,
        dose: Int,
    ) {
        logDao.insertAll(
            Log(
                uid = id ?: 0,
                supplementId = supplementId,
                time = time,
                dose = dose,
            )
        )
    }

    fun getSupplementWithLogs(): Flow<List<SupplementWithLog>> =
        logDao.getSupplementWithLogs()

}