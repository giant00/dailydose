package log

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Log(
    @PrimaryKey(autoGenerate = true) val uid: Long = 0,
    @ColumnInfo(name = "time") val time: Long,
    @ColumnInfo(name = "dose") val dose: Int,
    @ColumnInfo(name = "supplementId") val supplementId: Long,
)