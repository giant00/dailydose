package log

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface LogDao {
    @Query("SELECT * FROM log")
    fun getAll(): Flow<List<Log>>

    @Transaction
    @Query("SELECT * FROM supplement")
    fun getSupplementWithLogs(): Flow<List<SupplementWithLog>>

    @Query("SELECT * FROM log WHERE uid IN (:id)")
    suspend fun getById(id: Long): Log

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg logs: Log)

    @Delete
    suspend fun delete(log: Log)
}
