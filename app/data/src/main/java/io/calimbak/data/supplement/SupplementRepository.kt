package io.calimbak.data.supplement

import kotlinx.coroutines.flow.Flow

class SupplementRepository(
    private val supplementDao: SupplementDao
) {

    suspend fun save(
        id: Long,
        name: String,
        dose: Int,
    ) {
        supplementDao.insertAll(
            Supplement(
                uid = id,
                name = name,
                dose = dose
            )
        )
    }

    suspend fun getAll(): Flow<List<Supplement>> =
        supplementDao.getAll()

    suspend fun getById(id: Long): Supplement =
        supplementDao.getById(id)

}