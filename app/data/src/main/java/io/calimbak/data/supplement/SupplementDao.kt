package io.calimbak.data.supplement

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface SupplementDao {
    @Query("SELECT * FROM supplement")
    fun getAll(): Flow<List<Supplement>>

    @Query("SELECT * FROM supplement WHERE uid IN (:id)")
    suspend fun getById(id: Long): Supplement

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg supplements: Supplement)

    @Delete
    suspend fun delete(supplement: Supplement)
}
