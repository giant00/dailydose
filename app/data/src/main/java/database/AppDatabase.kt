package database

import androidx.room.Database
import androidx.room.RoomDatabase
import io.calimbak.data.supplement.Supplement
import io.calimbak.data.supplement.SupplementDao
import log.Log
import log.LogDao

@Database(entities = [Supplement::class, Log::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun supplementDao(): SupplementDao
    abstract fun logDao(): LogDao
}