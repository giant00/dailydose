package io.calimbak.dailydose

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalSavedStateRegistryOwner
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.*
import com.google.accompanist.navigation.animation.AnimatedNavHost
import com.google.accompanist.navigation.animation.composable
import com.google.accompanist.navigation.animation.navigation
import io.calimbak.dailydose.LeafScreen.Companion.EDIT_SUPPLEMENT_PARAM_ID
import io.calimbak.dailydose.LeafScreen.Companion.LOG_SUPPLEMENT_PARAM_ID
import io.calimbak.dailydose.day.Day
import io.calimbak.dailydose.day.DayViewModel
import io.calimbak.dailydose.dose.Dose
import io.calimbak.dailydose.dose.DoseViewModel
import io.calimbak.dailydose.editsupplement.EditSupplement
import io.calimbak.dailydose.editsupplement.EditSupplementViewModel
import io.calimbak.dailydose.logsupplement.LogSupplement
import io.calimbak.dailydose.logsupplement.LogSupplementViewModel

internal sealed class Screen(val route: String) {
    object Profile : Screen("profile")
    object Day : Screen("day")
    object Dose : Screen("dose")
}

public sealed class LeafScreen(private val route: String, internal val parent: Screen) {

    open fun route(): String =
        "${parent.route}/$route"

    object Day : LeafScreen("day", Screen.Day)
    object Dose : LeafScreen("dose", Screen.Dose)
    object EditSupplement : LeafScreen(
        "editSupplement?$EDIT_SUPPLEMENT_PARAM_ID={$EDIT_SUPPLEMENT_PARAM_ID}",
        Screen.Dose
    ) {
        fun createRoute(supplementId: Long?) =
            "${parent.route}/editSupplement".let {
                if (supplementId != null) "$it?$EDIT_SUPPLEMENT_PARAM_ID=$supplementId" else it
            }
    }

    object LogSupplement : LeafScreen("logSupplement/{$LOG_SUPPLEMENT_PARAM_ID}", Screen.Dose) {
        fun createRoute(supplementId: Long) =
            "${parent.route}/logSupplement/$supplementId"
    }

    companion object {
        const val LOG_SUPPLEMENT_PARAM_ID = "logSupplementId"
        const val EDIT_SUPPLEMENT_PARAM_ID = "editSupplementId"
    }
}

@ExperimentalAnimationApi
@Composable
internal fun AppNavigation(
    navController: NavHostController,
    modifier: Modifier = Modifier,
) {
    AnimatedNavHost(
        navController = navController,
        startDestination = Screen.Dose.route,
        modifier = modifier,
    ) {
        addDoseTopLevel(navController)
        addDayTopLevel(navController)
    }
}

@ExperimentalAnimationApi
private fun NavGraphBuilder.addDoseTopLevel(
    navController: NavController,
) {
//    Scoped VM
//    val loginBackStackEntry = remember { navController.getBackStackEntry("Parent") }
//    val loginViewModel: LoginViewModel = viewModel(loginBackStackEntry)
    navigation(
        route = Screen.Dose.route,
        startDestination = LeafScreen.Dose.route(),
    ) {
        addDose(navController)
        addEditSupplement(navController)
        addLogSupplement(navController)
    }
}

@ExperimentalAnimationApi
private fun NavGraphBuilder.addDayTopLevel(
    navController: NavController,
) {
    navigation(
        route = Screen.Day.route,
        startDestination = LeafScreen.Day.route(),
    ) {
        addDay(navController)
    }
}

@ExperimentalAnimationApi
private fun NavGraphBuilder.addDay(
    navController: NavController,
) {
    composable(LeafScreen.Day.route()) {
        val viewModel = viewModel<DayViewModel>()
        Day(
            viewModel = viewModel,
        )
    }
}

@ExperimentalAnimationApi
fun NavGraphBuilder.addEditSupplement(navController: NavController) {
    composable(
        route = LeafScreen.EditSupplement.route(),
        arguments = listOf(
            navArgument(EDIT_SUPPLEMENT_PARAM_ID) {
                type = NavType.StringType
                nullable = true
            }
        )) {
        val supplementId = it.arguments?.get(EDIT_SUPPLEMENT_PARAM_ID)?.toString()?.toLongOrNull()
        val viewModel = viewModel<EditSupplementViewModel>(
            factory = GenericSavedStateViewModelFactory(
                viewModelFactory = { savedStateHandle ->
                    EditSupplementViewModel(
                        savedStateHandle = savedStateHandle,
                        supplementId = supplementId,
                    )
                },
                owner = LocalSavedStateRegistryOwner.current
            )
        )
        EditSupplement(
            viewModel = viewModel,
            close = {
                navController.popBackStack()
            }
        )
    }
}

@ExperimentalAnimationApi
fun NavGraphBuilder.addLogSupplement(navController: NavController) {
    composable(
        route = LeafScreen.LogSupplement.route(),
        arguments = listOf(
            navArgument(LOG_SUPPLEMENT_PARAM_ID) { type = NavType.LongType },
        )
    ) {
        val supplementId = it.arguments?.get(LOG_SUPPLEMENT_PARAM_ID) as? Long?
        val viewModel = viewModel<LogSupplementViewModel>(
            factory = GenericSavedStateViewModelFactory(
                viewModelFactory = { savedStateHandle ->
                    LogSupplementViewModel(
                        savedStateHandle = savedStateHandle,
                        supplementId = supplementId
                    )
                },
                owner = LocalSavedStateRegistryOwner.current
            )
        )

        LogSupplement(
            viewModel = viewModel,
            openSupplementById = {
                navController.navigate(LeafScreen.EditSupplement.createRoute(it))
            },
            close = {
                navController.popBackStack()
            }
        )
    }
}

@ExperimentalAnimationApi
private fun NavGraphBuilder.addDose(
    navController: NavController,
) {
    composable(LeafScreen.Dose.route()) {
        val viewModel = viewModel<DoseViewModel>()
        Dose(
            viewModel = viewModel,
            openAddSupplement = {
                navController.navigate(LeafScreen.EditSupplement.createRoute(null))
            },
            openLogSupplementById = {
                navController.navigate(LeafScreen.LogSupplement.createRoute(it))
            }
        )
    }
}
