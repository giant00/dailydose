package io.calimbak.dailydose.day

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.layout.Arrangement.SpaceBetween
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import java.util.*


@OptIn(
    ExperimentalMaterialApi::class, androidx.compose.foundation.ExperimentalFoundationApi::class,
    androidx.compose.material3.ExperimentalMaterial3Api::class
)
@Composable
fun Day(
    viewModel: DayViewModel,
) {
    val state by viewModel.state.observeAsState()

    val logs: List<DayUiState.Log>? = state?.logs
    if (logs != null) {
        LazyColumn(
//            cells = GridCells.Adaptive(120.dp),
            contentPadding = PaddingValues(16.dp),
            modifier = Modifier.fillMaxWidth(),
        ) {
            logs.map { log ->
                item {
                    LogCard(log, { viewModel.expand(it) })
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun LogCard(
    log: DayUiState.Log,
    expandSupplement: (Long) -> Unit,
    modifier: Modifier = Modifier,
) {
    Card(
        onClick = {
            expandSupplement(log.supplementId)
        },
        modifier = modifier
            .padding(horizontal = 4.dp, vertical = 4.dp),
        backgroundColor = MaterialTheme.colorScheme.surfaceVariant
    ) {
        Column(
            modifier = Modifier
                .padding(horizontal = 4.dp, vertical = 4.dp),
        ) {
            Text(
                text = log.supplementName,
                color = MaterialTheme.colorScheme.onSurfaceVariant,
                style = MaterialTheme.typography.titleMedium,
            )

            if (log.isExpanded) {
                log.intake.map {
                    IntakeText(intake = it)
                }
            } else {
                log.intake.firstOrNull()?.let {
                    IntakeText(intake = it)
                }
            }
        }
    }
}

@Composable
fun IntakeText(intake: DayUiState.Intake) {
    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = SpaceBetween
    ) {
        val date = Date(intake.time)
        val dateFormat = android.text.format.DateFormat.getDateFormat(LocalContext.current.applicationContext)
        val timeFormat = android.text.format.DateFormat.getTimeFormat(LocalContext.current.applicationContext)
        Text(
            text = intake.dose.toString(),
            color = MaterialTheme.colorScheme.onSurfaceVariant,
            style = MaterialTheme.typography.bodyMedium,
        )
        Text(
            text = "${dateFormat.format(date)} ${timeFormat.format(date)}",
            color = MaterialTheme.colorScheme.onSurfaceVariant,
            style = MaterialTheme.typography.bodyMedium,
        )
    }
}

@Preview
@Composable
fun PreviewLogCard() {
    LogCard(
        DayUiState.Log(
            intake = listOf(
                DayUiState.Intake(1234, 1000)
            ),
            supplementId = 12,
            supplementName = "B12",
            isExpanded = true,
        ),
        {}
    )
}