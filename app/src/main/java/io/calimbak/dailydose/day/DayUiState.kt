package io.calimbak.dailydose.day

data class DayUiState(
    val logs: List<Log>
) {

    data class Log(
        val intake: List<Intake>,
        val supplementName: String,
        val supplementId: Long,
        val isExpanded: Boolean = false,
    )

    data class Intake(
        val time: Long,
        val dose: Int,
    )

}