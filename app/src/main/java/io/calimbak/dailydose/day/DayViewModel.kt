package io.calimbak.dailydose.day

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.calimbak.dailydose.manualdi.RepositoryCreator
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import log.LogRepository
import log.SupplementWithLog

class DayViewModel(
    private val logRepository: LogRepository = RepositoryCreator.createLogRepository(),
) : ViewModel() {

    private val _state = MutableLiveData<DayUiState>()
    val state: LiveData<DayUiState> = _state

    private val expandedIds: MutableStateFlow<Set<Long>> = MutableStateFlow(mutableSetOf())

    init {
        viewModelScope.launch {
            combine(
                flow = logRepository.getSupplementWithLogs(),
                flow2 = expandedIds,
                transform = { supplements: List<SupplementWithLog>, expandedIds: Set<Long> ->
                    DayUiState(
                        logs = supplements.map {
                            DayUiState.Log(
                                intake = it.logs.reversed().map { log ->
                                    DayUiState.Intake(
                                        time = log.time,
                                        dose = log.dose,
                                    )
                                },
                                isExpanded = expandedIds.contains(it.supplement.uid),
                                supplementName = it.supplement.name.trim(),
                                supplementId = it.supplement.uid,
                            )
                        }
                    )
                }
            ).stateIn(
                scope = viewModelScope,
                started = SharingStarted.WhileSubscribed(5000),
                initialValue = DayUiState(emptyList()),
            ).collect {
                _state.value = it
            }
        }
    }

    fun expand(supplementId: Long) {
        expandedIds.updateAndGet { set ->
            if (set.contains(supplementId)) {
                set - supplementId
            } else {
                set + supplementId
            }
        }
    }

}