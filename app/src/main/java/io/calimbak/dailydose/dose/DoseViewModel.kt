package io.calimbak.dailydose.dose

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.calimbak.dailydose.manualdi.RepositoryCreator
import io.calimbak.data.supplement.SupplementRepository
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class DoseViewModel(
    private val addSupplementRepository: SupplementRepository = RepositoryCreator.createSupplementRepository()
) : ViewModel() {

    private val _state = MutableLiveData<DoseUiState>()
    val state: LiveData<DoseUiState> = _state

    init {
        viewModelScope.launch {
            addSupplementRepository.getAll().collect { supplements ->
                _state.value = DoseUiState(
                    supplements = supplements.map {
                        DoseUiState.Supplement(
                            id = it.uid,
                            name = it.name.trim(),
                            dose = it.dose.toString()
                        )
                    }
                )
            }
        }
    }

    fun openSupplement(supplementId: Long) {

    }

}