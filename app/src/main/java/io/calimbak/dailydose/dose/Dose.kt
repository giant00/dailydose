package io.calimbak.dailydose.dose

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import io.calimbak.dailydose.R

@OptIn(
    ExperimentalMaterialApi::class, androidx.compose.foundation.ExperimentalFoundationApi::class,
    androidx.compose.material3.ExperimentalMaterial3Api::class
)
@Composable
fun Dose(
    viewModel: DoseViewModel,
    openAddSupplement: () -> Unit,
    openLogSupplementById: (Long) -> Unit,
) {
    val state by viewModel.state.observeAsState()

    val supplements: List<DoseUiState.Supplement>? = state?.supplements
    Scaffold(
        floatingActionButton = {
            FabSupplement { openAddSupplement() }
        }
    ) {
        if (supplements != null) {
            LazyVerticalGrid(
                cells = GridCells.Adaptive(120.dp),
                contentPadding = PaddingValues(16.dp)
            ) {
                supplements.map {
                    item {
                        SupplementCard(it, openLogSupplementById)
                    }
                }
            }
        }
    }
}

@Composable
fun FabSupplement(onFabClick: () -> Unit) {
    FloatingActionButton(onClick = onFabClick) {
        Icon(
            imageVector = Icons.Default.Add,
            contentDescription = stringResource(R.string.contentDescription_iconAdd)
        )
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun SupplementCard(
    supplement: DoseUiState.Supplement,
    openLogSupplementById: (Long) -> Unit,
    modifier: Modifier = Modifier,
) {
    Card(
        onClick = {
            openLogSupplementById(supplement.id)
        },
        modifier = modifier.padding(horizontal = 4.dp, vertical = 4.dp),
        backgroundColor = MaterialTheme.colorScheme.surfaceVariant
    ) {
        Column(
            modifier = Modifier.padding(horizontal = 4.dp, vertical = 4.dp),
        ) {
            Text(
                text = supplement.name,
                color = MaterialTheme.colorScheme.onSurfaceVariant,
                style = MaterialTheme.typography.titleMedium,
            )
            Text(
                text = supplement.dose,
                modifier = Modifier.padding(top = 2.dp),
                color = MaterialTheme.colorScheme.onSurfaceVariant,
                style = MaterialTheme.typography.bodyMedium,
            )
        }

    }
}