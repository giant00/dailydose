package io.calimbak.dailydose.dose

data class DoseUiState(
    val supplements: List<Supplement>
) {
    data class Supplement(
        val id: Long,
        val name: String,
        val dose: String,
    )
}