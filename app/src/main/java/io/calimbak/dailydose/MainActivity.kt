package io.calimbak.dailydose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.ExperimentalAnimationApi
import io.calimbak.dailydose.helper.DatabaseInitializer
import io.calimbak.dailydose.home.Home
import io.calimbak.dailydose.ui.theme.DailyDoseTheme

class MainActivity : ComponentActivity() {

    @OptIn(ExperimentalAnimationApi::class,
        androidx.compose.material3.ExperimentalMaterial3Api::class
    )
    override fun onCreate(savedInstanceState: Bundle?) {
        DatabaseInitializer.initialize(applicationContext)

        super.onCreate(savedInstanceState)
        setContent {
            DailyDoseTheme {
                Home()
            }
        }
    }
}