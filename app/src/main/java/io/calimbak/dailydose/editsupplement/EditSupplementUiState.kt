package io.calimbak.dailydose.editsupplement

data class EditSupplementUiState(
    val name: String? = null,
    val dose: Int? = null,
) {

    fun isSavable(): Boolean =
        !name.isNullOrEmpty() && dose != null

}