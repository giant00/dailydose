package io.calimbak.dailydose.editsupplement

import android.util.Log
import androidx.annotation.StringRes
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import io.calimbak.dailydose.R

@Composable
fun EditSupplement(
    viewModel: EditSupplementViewModel,
    close: () -> Unit,
) {
    val state by viewModel.state.observeAsState()

    if (state == null) {
        // TODO: Show loading?
        return
    }

    Surface(
        modifier = Modifier.fillMaxWidth()
            .height(IntrinsicSize.Min)
            .padding(horizontal = 8.dp, vertical = 16.dp)
    ) {
        Column {
            SupplementInputField(
                value = state?.name,
                inputLabel = R.string.addSupplement_name,
                onInputChanged = { viewModel.updateName(it) },
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Text),
                modifier = Modifier.fillMaxWidth()
            )

            Spacer(modifier = Modifier.height(4.dp))

            SupplementInputField(
                value = state?.dose?.toString() ?: "",
                inputLabel = R.string.addSupplement_dose,
                onInputChanged = { viewModel.updateDose(it?.toIntOrNull()) },
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                modifier = Modifier.fillMaxWidth()
            )

            Spacer(modifier = Modifier.height(16.dp))

            Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.CenterEnd
            ) {
                Button(
                    enabled = state?.isSavable() ?: false,
                    onClick = {
                        viewModel.save()
                        close()
                    },
                ) {
                    Text(
                        text = stringResource(id = R.string.addSupplement_create),
                        color = LocalContentColor.current
                    )
                }
            }
        }
    }
}

@Composable
internal fun SupplementInputField(
    value: String?,
    @StringRes inputLabel: Int,
    onInputChanged: (String?) -> Unit,
    modifier: Modifier = Modifier,
    keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
) {
    var input by remember { mutableStateOf(TextFieldValue(value ?: "")) }
    TextField(
        value = input,
        onValueChange = { newValue ->
            input = newValue
            onInputChanged(newValue.text)
        },
        singleLine = true,
        keyboardOptions = keyboardOptions,
        label = {
            Text(
                text = stringResource(id = inputLabel),
            )
        },
        colors = TextFieldDefaults.textFieldColors(textColor = LocalContentColor.current),
        modifier = modifier,
    )
}
