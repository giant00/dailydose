package io.calimbak.dailydose.editsupplement

import androidx.lifecycle.*
import io.calimbak.dailydose.LeafScreen
import io.calimbak.dailydose.manualdi.RepositoryCreator
import io.calimbak.data.supplement.SupplementRepository
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class EditSupplementViewModel(
    savedStateHandle: SavedStateHandle,
    private val supplementId: Long? = null,
    private val editSupplementRepository: SupplementRepository = RepositoryCreator.createSupplementRepository()
) : ViewModel() {

    private val _state = MutableLiveData<EditSupplementUiState>()
    val state: LiveData<EditSupplementUiState> = _state

    init {
        if (supplementId == null) {
            _state.value = EditSupplementUiState()
        } else {
            viewModelScope.launch {
                val supplement =  editSupplementRepository.getById(supplementId)
                _state.postValue(
                    EditSupplementUiState(
                        name = supplement.name,
                        dose = supplement.dose,
                    )
                )
            }
        }
    }

    fun updateName(name: String?) {
        _state.value = _state.value?.copy(
            name = name
        )
    }

    fun updateDose(dose: Int?) {
        _state.value = _state.value?.copy(
            dose = dose
        )
    }

    fun save() {
        val current = _state.value
        if (current?.isSavable() == true) {
            viewModelScope.launch {
                editSupplementRepository.save(
                    id = supplementId ?: 0,
                    name = current.name!!,
                    dose = current.dose!!,
                )
            }
        }
    }

}