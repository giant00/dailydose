package io.calimbak.dailydose.manualdi

import io.calimbak.dailydose.helper.DatabaseInitializer
import io.calimbak.data.supplement.SupplementRepository
import log.LogRepository

object RepositoryCreator {

    fun createSupplementRepository(): SupplementRepository =
        SupplementRepository(
            supplementDao = DatabaseInitializer.database.supplementDao()
        )

    fun createLogRepository(): LogRepository =
        LogRepository(
            logDao = DatabaseInitializer.database.logDao(),
            supplementDao = DatabaseInitializer.database.supplementDao(),
        )

}