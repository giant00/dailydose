package io.calimbak.dailydose.helper

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import database.AppDatabase

object DatabaseInitializer {

    private lateinit var db: AppDatabase
    val database: AppDatabase
        get() = db

    fun initialize(
        applicationContext: Context
    ) {
        db = Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, "daily-dose-db"
        ).build()
    }

}