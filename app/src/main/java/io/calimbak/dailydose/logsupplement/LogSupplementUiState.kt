package io.calimbak.dailydose.logsupplement

data class LogSupplementUiState(
    val id: Long,
    val name: String,
    val dose: Int,
)