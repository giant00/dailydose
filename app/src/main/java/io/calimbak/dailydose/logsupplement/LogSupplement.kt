package io.calimbak.dailydose.logsupplement

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import io.calimbak.dailydose.R
import io.calimbak.dailydose.dose.DoseUiState
import io.calimbak.dailydose.dose.SupplementCard
import io.calimbak.dailydose.editsupplement.SupplementInputField

@Composable
fun LogSupplement(
    viewModel: LogSupplementViewModel,
    openSupplementById: (Long) -> Unit,
    close: () -> Unit,
) {
    val state: LogSupplementUiState? by viewModel.state.observeAsState()

    if (state == null) {
        // TODO: Show loading?
        return
    }

    LogSupplementScreen(
        state = state!!,
        openSupplementById = openSupplementById,
        saveDose = { dose ->
            viewModel.log(dose)
        },
        close = close
    )
}

@Composable
internal fun LogSupplementScreen(
    state: LogSupplementUiState,
    openSupplementById: (Long) -> Unit,
    saveDose: (Int) -> Unit,
    close: () -> Unit,
) {
    Surface(
        modifier = Modifier.fillMaxWidth()
            .height(IntrinsicSize.Min)
            .padding(horizontal = 8.dp, vertical = 16.dp)
    ) {
        Column(
            modifier = Modifier.fillMaxWidth()
        ) {
            SupplementCard(
                supplement = DoseUiState.Supplement(
                    id = state.id,
                    name = state.name,
                    dose = state.dose.toString(),
                ),
                openLogSupplementById = {
                    openSupplementById(it)
                },
                modifier = Modifier.fillMaxWidth()
            )

            Spacer(modifier = Modifier.height(4.dp))

            var doseInput by remember { mutableStateOf(state.dose.toString()) }
            SupplementInputField(
                value = doseInput,
                inputLabel = R.string.addSupplement_dose,
                onInputChanged = { doseInput = it ?: "" },
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                modifier = Modifier.fillMaxWidth()
            )

            Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.CenterEnd
            ) {
                Button(
                    enabled = doseInput.isNotBlank(),
                    onClick = {
                        saveDose(doseInput.toInt())
                        close()
                    },
                ) {
                    Text(
                        text = stringResource(id = R.string.logSupplement_log),
                        color = LocalContentColor.current
                    )
                }
            }
        }
    }
}

@Composable
@Preview
private fun PreviewLogSupplementScreen() {
    LogSupplementScreen(
        state = LogSupplementUiState(
            id = 42,
            name = "B42",
            dose = 42
        ),
        openSupplementById = {},
        saveDose = {},
        close = {}
    )
}