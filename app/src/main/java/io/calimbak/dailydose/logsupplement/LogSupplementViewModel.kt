package io.calimbak.dailydose.logsupplement

import androidx.lifecycle.*
import io.calimbak.dailydose.manualdi.RepositoryCreator
import io.calimbak.data.supplement.SupplementRepository
import kotlinx.coroutines.launch
import log.LogRepository

class LogSupplementViewModel(
    savedStateHandle: SavedStateHandle,
    private val supplementId: Long? = null,
    private val supplementRepository: SupplementRepository = RepositoryCreator.createSupplementRepository(),
    private val logRepository: LogRepository = RepositoryCreator.createLogRepository(),
) : ViewModel() {

    private val _state = MutableLiveData<LogSupplementUiState>()
    val state: LiveData<LogSupplementUiState> = _state

    init {
        if (supplementId == null) {
            // TODO: Handle that / open add new?
        } else {
            viewModelScope.launch {
                val supplement = supplementRepository.getById(supplementId)
                _state.postValue(
                    LogSupplementUiState(
                        name = supplement.name,
                        dose = supplement.dose,
                        id = supplementId
                    )
                )
            }
        }
    }

    fun log(dose: Int) {
        viewModelScope.launch {
            logRepository.save(
                id = null,
                dose = dose,
                supplementId = supplementId!!,
                time = System.currentTimeMillis(),
            )
        }
    }

}