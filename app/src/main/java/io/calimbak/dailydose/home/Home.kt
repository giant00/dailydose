package io.calimbak.dailydose.home

import androidx.compose.animation.Crossfade
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.outlined.AddCircle
import androidx.compose.material.icons.outlined.AddCircleOutline
import androidx.compose.material.icons.outlined.Home
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavController
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.NavGraph.Companion.findStartDestination
import com.google.accompanist.insets.LocalWindowInsets
import com.google.accompanist.insets.rememberInsetsPaddingValues
import com.google.accompanist.insets.ui.BottomNavigation
import com.google.accompanist.navigation.animation.rememberAnimatedNavController
import io.calimbak.dailydose.AppNavigation
import io.calimbak.dailydose.R
import io.calimbak.dailydose.Screen

@OptIn(ExperimentalAnimationApi::class, ExperimentalMaterial3Api::class)
@Composable
fun Home() {
    val navController = rememberAnimatedNavController()
    Scaffold(
        bottomBar = {
            val currentSelectedItem by navController.currentScreenAsState()
            HomeBottomNavigation(
                selectedNavigation = currentSelectedItem,
                onNavigationSelected = { selected ->
                    navController.navigate(selected.route) {
                        launchSingleTop = true
                        restoreState = true

                        popUpTo(navController.graph.findStartDestination().id) {
                            saveState = true
                        }
                    }
                },
                modifier = Modifier.fillMaxWidth()
            )
        },
        modifier = Modifier.fillMaxSize(),
    ) { paddingValues: PaddingValues ->
        AppNavigation(
            navController = navController,
            modifier = Modifier
                .fillMaxSize()
                .padding(paddingValues),
        )
    }
}

@Composable
internal fun HomeBottomNavigation(
    selectedNavigation: Screen,
    onNavigationSelected: (Screen) -> Unit,
    modifier: Modifier = Modifier,
) {
    BottomNavigation(
        backgroundColor = MaterialTheme.colors.surface,
        contentPadding = rememberInsetsPaddingValues(LocalWindowInsets.current.navigationBars),
        contentColor = contentColorFor(MaterialTheme.colors.surface),
        modifier = modifier
    ) {
        HomeNavigationItem(
            normalIcon = Icons.Filled.Home,
            selectedIcon = Icons.Outlined.Home,
            contentDescription = stringResource(id = R.string.contentDescription_homeDailyIcon),
            label = stringResource(id = R.string.homeDailyLabel),
            selected = selectedNavigation.route.startsWith(Screen.Day.route),
            onClick = { onNavigationSelected(Screen.Day) },
        )
        HomeNavigationItem(
            normalIcon = Icons.Outlined.AddCircle,
            selectedIcon = Icons.Outlined.AddCircleOutline,
            contentDescription = stringResource(id = R.string.contentDescription_homeDoseIcon),
            label = stringResource(id = R.string.homeDoseLabel),
            selected = selectedNavigation.route.startsWith(Screen.Dose.route),
            onClick = { onNavigationSelected(Screen.Dose) },
        )
    }
}

@Composable
internal fun RowScope.HomeNavigationItem(
    normalIcon: ImageVector,
    selectedIcon: ImageVector,
    contentDescription: String,
    label: String,
    selected: Boolean,
    onClick: () -> Unit,
) {
    BottomNavigationItem(
        icon = {
            HomeNavigationIcon(
                normalIcon = normalIcon,
                selectedIcon = selectedIcon,
                contentDescription = contentDescription,
                selected = selected,
            )
        },
        label = { Text(text = label) },
        selected = selected,
        onClick = onClick,
    )
}

@Composable
internal fun HomeNavigationIcon(
    normalIcon: ImageVector,
    selectedIcon: ImageVector,
    contentDescription: String,
    selected: Boolean
) {
    Crossfade(targetState = selected) {
        Icon(
            painter = rememberVectorPainter(image = if (it) normalIcon else selectedIcon),
            contentDescription = contentDescription,
//            tint = if (it) MaterialTheme.colors.secondary else MaterialTheme.colors.onBackground
        )
    }
}

/**
 * Adds an [NavController.OnDestinationChangedListener] to this [NavController] and updates the
 * returned [State] which is updated as the destination changes.
 */
@Stable
@Composable
private fun NavController.currentScreenAsState(): State<Screen> {
    val selectedItem = remember { mutableStateOf<Screen>(Screen.Day) }

    DisposableEffect(this) {
        val listener = NavController.OnDestinationChangedListener { _, destination, _ ->
            when {
                destination.hierarchy.any { it.route == Screen.Day.route } -> {
                    selectedItem.value = Screen.Day
                }
                destination.hierarchy.any { it.route == Screen.Dose.route } -> {
                    selectedItem.value = Screen.Dose
                }
            }
        }
        addOnDestinationChangedListener(listener)

        onDispose {
            removeOnDestinationChangedListener(listener)
        }
    }

    return selectedItem
}